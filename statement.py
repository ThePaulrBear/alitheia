from typing import Dict, List, Union, Set
from uuid import UUID, uuid4

import sympy


class Reference:
    uuid: UUID = uuid4()
    target = None

    def __init__(self, target):
        self.target = target


class ReferenceResolver:
    uuid_table: Dict[UUID, Reference] = {}

    def add_reference(self, reference: Reference):
        self.uuid_table[reference.uuid] = reference

    def resolve_target_from_uuid(self, uuid):
        return self.uuid_table[uuid].target


class StatementReference:

    def __init__(self, statement_uuid):
        self.statement_uuid = statement_uuid


class Statement:
    ContentListType = Union[str, List[Union[str, StatementReference]]]
    # text: ContentListType
    # self.uuid: UUID

    def __init__(self, text: ContentListType, uuid=None):
        self.text = text
        self.uuid: str = Statement._sanitize_uuid(uuid)

    @staticmethod
    def _uuid_to_string(uuid: UUID):
        return "_" + str(uuid.hex)

    @staticmethod
    def _sanitize_uuid(uuid):
        if uuid is None:
            uuid = uuid4()

        if isinstance(uuid, str):
            # We ultimately want a string, but this checks
            # to make sure the string is in a uuid compatible format
            if uuid[0] is "_":
                # Trim underscore
                uuid = uuid[1:]
            uuid = UUID(uuid)
        elif isinstance(uuid, int):
            uuid = UUID(int=uuid)
        elif not isinstance(uuid, UUID):
            raise ValueError("Not a valid type for UUID: %s" % uuid)

        return "_" + str(uuid.hex)

    def __eq__(self, other):
        if not isinstance(other, Statement):
            return False

        if not self.uuid == other.uuid:
            return False
        if not self.text == other.text:
            return False

        return True

    def __hash__(self):
        return hash(self.uuid) + hash(self.text)

    def __str__(self):
        return "Statement{text:%s, uuid:%s}" % (self.text, str(self.uuid))

    def __repr__(self):
        return "{%s, uuid:%s}" % (self.text, str(self.uuid)[1:6])


class TruthStatement(Statement, sympy.symbol.Symbol):
    """ An invariant data type that stores a statement that has a truth value (it can be either true or false).
    """

    def __init__(self, text: str, uuid=None):
        Statement.__init__(self, text, uuid)
        sympy.symbol.Expr.__init__(self)

        # super().__init__(text, uuid)
        # self.boolean = sympy.symbol.Symbol(text)

    def __new__(cls, text: str, uuid=None):
        if uuid is None:
            uuid = uuid4()
        # TODO: Remove truncation from uuid string.
        # return super().__new__(cls, str(uuid))
        return super().__new__(cls, text) # + "_" + str(uuid)[0:6])

    def __eq__(self, other):
        eq__ = Statement.__eq__(self, other)
        # print("TruthStatement.__eq__?", eq__)
        # print("self:", self)
        # print("other:", other)
        return eq__

    def __hash__(self):
        return Statement.__hash__(self)

    # def __and__(self, other):
    #     return self.boolean.__and__(other)
    #
    # def __or__(self, other):
    #     return self.boolean.__or__(other)
    #
    # def __invert__(self):
    #     return self.boolean.__invert__()
    #
    # def __rshift__(self, other):
    #     return self.boolean.__rshift__(other)
    #
    # def __lshift__(self, other):
    #     return self.boolean.__lshift__(other)
    #
    # def __xor__(self, other):
    #     return self.boolean.__xor__(other)

    def __str__(self):
        return self.text
        # return "TruthStatement{%s, uuid:%s}" % (self.text, str(self.uuid)[1:6])

    def __repr__(self):
        return self.text
        # return "{%s, uuid:%s}" % (self.text, str(self.uuid)[1:6])


class Definition(Statement):
    term: str

    def __init__(self, term, definition, uuid=None):
        super(Definition, self).__init__(definition, uuid)
        self.term = term

    pass


class Proof:
    hypotheses: TruthStatement
    conclusion: TruthStatement
    justification: str

    def __init__(self,
                 conclusion: TruthStatement,
                 hypotheses: TruthStatement = None,
                 justification: str = ""):
        # assert isinstance(conclusion, TruthStatement), \
        #     "Conclusion was a %s" % type(conclusion)
        self.conclusion = conclusion
        if hypotheses is None:
            self.hypotheses = sympy.true
        else:
            assert isinstance(hypotheses, sympy.Basic), "Hypothesis must be a symy object. Was %s" % hypotheses
            self.hypotheses = hypotheses
        self.justification = justification

    def __eq__(self, other):
        if not isinstance(other, Proof):
            return False

        if not self.hypotheses == other.hypotheses:
            return False
        if not self.conclusion == other.conclusion:
            return False
        if not self.justification == other.justification:
            return False

        return True

    def __hash__(self):
        sum = hash(self.conclusion) \
              + hash(self.hypotheses) \
              + hash(self.justification)
        # for hypo in self.hypotheses:
        #     sum += hash(hypo)

        return sum

    def __str__(self):
        return "Proof{conclusion:%s, hypotheses: %s, justification:%s}" \
               % (self.conclusion, self.hypotheses, self.justification)

    def __repr__(self):
        return "%s => %s" % (self.hypotheses, self.conclusion)


class DefinitionUnwinder:
    _definition_pool: Dict[UUID, Definition] = {}
    _definition_terms: Dict[str, Definition] = {}

    regex = "###.+###"

    def add_definition(self, definition):
        if self._definition_pool.get(definition.uuid) is not None:
            print("Duplicate entry: ", definition.term, definition.text, definition.uuid)
        else:
            self._definition_pool[definition.uuid] = definition
            self._definition_terms[definition.term] = definition

    def unwind(self, definition):
        expanded_def = self._definition_terms[definition].text

        for i in range(1, 4):
            for key, value in self._definition_terms.items():
                expanded_def = expanded_def.replace("###%s###" % key, value.text)
        print(expanded_def)


class BooleanVariable(sympy.symbol.Boolean):
    pass


def proof_tree(conclusion: TruthStatement) -> str:
    print(conclusion)

# if __name__ == '__main__':

#
# truth_statements: List[str] = {
#
# }

# truth_tracer = TruthTracer()
# truth1 = TruthStatement("not(P or not Q) is equivalent to not P or Q")
#
# truth_tracer.add_truth_statement(truth1)
# truth_tracer.add_proof(Proof(hypotheses=set(),  # hypotheses=["P is a truth statement", "Q is a truth statement"],
#                              conclusion=truth1,
#                              justification="not(P or not Q) is equivalent to not P and not not Q \n"
#                                            "which is equivalent not P and Q"
#                              ))
# truth_tracer.add_proof(Proof(hypotheses=set(),
#                              conclusion=truth1,
#                              justification="We can prove this by enumerating the possibilities on a truth table"
#                              ))
#
# truth_tracer.get_proofs(truth1)
#
# truth2 = TruthStatement("A ###boolean value### cannot be both ###true### and false")
# truth_tracer.add_truth_statement(truth2)
# truth_tracer.add_proof(Proof(
#     hypotheses=set(), conclusion=truth2, justification="Self evident"
# ))
# truth_tracer.add_proof(Proof(
#     hypotheses=set(), conclusion=truth2, justification
#     ="If a statement is tre and false, then we have a contradiction"))
#
# truth_tracer.get_proofs(truth1)
#
# inauguration_image = Definition("The Image", "An photograph the crowds at ###Trump's Inauguration### seen <here>")
#
# image_of_inauguration_crowd = TruthStatement("The Trump White House released ###The Image###")
# camera_location_crowd = TruthStatement("###The Image### was taken at the capitol steps from an altitude of H")
# google_maps = TruthStatement("Google Maps shows the distance from the Capitol is D feet")
# optical_path = TruthStatement("The path of light rays are well approximated by straight lines")
# average_man_height = TruthStatement("The average height of an American male is H")
# law_of_similar_triangles = TruthStatement("If two triangles are equal, then the ratios "
#                                           "between corresponding sides are equal.")
#
# crowd_size = TruthStatement("The crowd at Trump's Inauguration extended to point X.")
# crowd_size_proof = Proof(crowd_size, hypotheses={image_of_inauguration_crowd, google_maps, optical_path})
#
# axioms: Dict[str, str] = {
#     "One or the other": "A value cannot be both true and false"
# }
#
# definitions: Dict[str, str] = {
#     "true": "in accordance with ##fact## or reality",
#     "false": "not ###true###",
#     "Boolean value": "A ###value### that is either ###true### or ###false###."
# }
#
# definition_unwinder = DefinitionUnwinder()
# for key, value in definitions.items():
#     definition_unwinder.add_definition(Definition(key, value))
#
# definition_unwinder.unwind("Boolean value")

# for key, value in axioms.items():
#     hierarchy.add(TruthStatement(key, value))
#

####################
# Desired behavior #
####################

# 1. Expand definitions.

# 2. For a given truth statement, trace back the logic to accepted truths.
# That is...
# Define a set of truth statements. Let one be the conclusion. Create a directed non-cyclical graph of the truth
# statements that ends with the conclusion in the terminal node. Between each node, have any number of graph edges.
# Each edge represents an argument that takes the origin nodes as given and the
