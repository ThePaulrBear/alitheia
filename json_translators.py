import json
from typing import Dict
from uuid import UUID

from statement import TruthStatement, Proof
from truth_tracer import TruthTracer


class LogicJSONEncoder(json.JSONEncoder):
    _supported_types = {TruthStatement, Proof, TruthTracer}

    def default(self, obj):
        print("to_json(%s)" % obj)
        if isinstance(obj, dict):
            print("such a DICT")
            return "JUNK STRING"
        # if isinstance(obj, UUID):
        #     return str(obj)
        if isinstance(obj, set):
            return list(obj)
        if isinstance(obj, TruthTracer):
            tt = obj
            truth_statements = tt.get_truth_statements()
            proofs = tt.get_all_proofs()

            return {"_type": "TruthTracer",
                    "value": {
                        "_truth_statements": truth_statements,
                        "_proofs": proofs,
                        "_definitions": None
                    }}

        for a_type in self._supported_types:
            if isinstance(obj, a_type):
                # print("is a ", a_type)
                # print("contains:", obj.__dict__)
                cleaned_dict = self.convert_uuid_keys_to_str(obj.__dict__)
                # print("cleaned_dict:", cleaned_dict)
                return {"_type": a_type.__name__,
                        "value": cleaned_dict}

        return json.JSONEncoder.default(self, obj)

    def convert_uuid_keys_to_str(self, dictionary: Dict):
        if not isinstance(dictionary, dict):
            return dictionary

        clean = {}
        for key, value in dictionary.items():
            # If the key is a TruthStatement, then extract the UUID
            if isinstance(key, TruthStatement):
                key = key.uuid

            # if isinstance(key, UUID):
            #     print("key:", key, "str(key):", str(key))
            #     key = str(key)
            # el
            if not isinstance(key, str):
                print(key)
                raise ValueError("Key must be UUID or str. Was: %s" % key)

            assert isinstance(key, str)

            clean[key] = self.convert_uuid_keys_to_str(value)
        return clean


        # if not isinstance(dictionary, dict):
        #     return dictionary
        #
        # clean = {}
        # for key, value in dictionary.items():
        #     # If the key is a TruthStatement, then extract the UUID
        #     if isinstance(key, TruthStatement):
        #         key = key.uuid
        #
        #     # if isinstance(key, UUID):
        #     #     print("key:", key, "str(key):", str(key))
        #     #     key = str(key)
        #     # el
        #
        #     assert isinstance(key, str), "Key must be UUID or str. Was: %s" % key
        #
        #     clean[key] = self.convert_uuid_keys_to_str(value)
        # return clean


class LogicJSONDecoder(json.JSONDecoder):

    def __init__(self, *args, **kwargs):
        json.JSONDecoder.__init__(self,
                                  object_hook=self.object_hook,
                                  *args, **kwargs)

    # noinspection PyMethodMayBeStatic
    def object_hook(self, obj):
        # print("object:", type(obj), obj)

        if isinstance(obj, str):
            return obj
        if '_type' not in obj:
            return obj

        obj_type = obj['_type']
        values = obj['value']
        if obj_type == TruthStatement.__name__:
            return TruthStatement(values['text'],
                                  uuid=values['uuid'])
        if obj_type == Proof.__name__:
            return Proof(values['conclusion'],
                         values['hypotheses'],
                         values['justification'])

        if obj_type == TruthTracer.__name__:
            return TruthTracer(truth_statements=values['_truth_statements'],
                               proofs=values['_proofs'],
                               definitions=values['_definitions'])

        return obj


def to_json(obj, indent: int = None):
    # LogicJSONEncoder.convert_uuid_keys_to_str()
    return json.dumps(obj, cls=LogicJSONEncoder, indent=indent)


def from_json(json_str: str):
    return json.loads(json_str, cls=LogicJSONDecoder)
