# -*- coding: utf-8 -*-
"""
Created on Mon Dec 8 2019

@author: Paul Wintz
"""
import unittest
from statement import Statement
from uuid import uuid4, UUID


class TestSanitizeUUID(unittest.TestCase):

    def test_None(self):
        sanitized_uuid = Statement._sanitize_uuid(None)
        self.assertIsInstance(sanitized_uuid, str)

    def test_UUID(self):
        uuid = uuid4()
        sanitized_uuid = Statement._sanitize_uuid(uuid)
        self.assertIsInstance(sanitized_uuid, str)
        self.assertEqual(sanitized_uuid, Statement._uuid_to_string(uuid))

    def test_str(self):
        uuid_str = Statement._uuid_to_string(UUID("3861c805-86ca-4bd0-808e-bee617fb68b9"))
        sanitized_uuid = Statement._sanitize_uuid(uuid_str)
        self.assertIsInstance(sanitized_uuid, str)
        self.assertEqual(sanitized_uuid, uuid_str)

    def test_int(self):
        uuid = 1234
        sanitized_uuid = Statement._sanitize_uuid(uuid)
        self.assertIsInstance(sanitized_uuid, str)
        self.assertEqual(sanitized_uuid, Statement._uuid_to_string(UUID(int=uuid)))

    def test_unsupported_type(self):
        uuid = object()
        self.assertRaises(ValueError, Statement._sanitize_uuid, uuid)

    def test_bad_string(self):
        uuid = "a happy day in Hell"
        self.assertRaises(ValueError, Statement._sanitize_uuid, uuid)


class TestStatement(unittest.TestCase):

    def test_init(self):
        statement = Statement("Blah blah blah")
        self.assertEqual(statement.text, "Blah blah blah")
        self.assertIsInstance(statement.uuid, str)

    def test_eq_self(self):
        statement = Statement("We hold these truths")
        self.assertEqual(statement, statement)

    def test_eq_same_text_diff_uuid_1(self):
        same_text_diff_uuid_1 = Statement("Same text, different UUID")
        same_text_diff_uuid_2 = Statement("Same text, different UUID")

        self.assertNotEqual(same_text_diff_uuid_1, same_text_diff_uuid_2)
        self.assertNotEqual(same_text_diff_uuid_2, same_text_diff_uuid_1)

    def test_eq_same_text_and_uuid(self):
        same_text_and_uuid_1 = Statement("hello", uuid=1)
        same_text_and_uuid_2 = Statement("hello", uuid=1)

        self.assertEqual(same_text_and_uuid_1, same_text_and_uuid_2)


if __name__ == '__main__':
    unittest.main()
