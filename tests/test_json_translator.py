# -*- coding: utf-8 -*-
import unittest
import database
from statement import TruthStatement, Proof
from truth_tracer import TruthTracer
import json_translators


class TestJsonTranslator(unittest.TestCase):

    def test_json_truth_statement(self):
        statement = TruthStatement("hello")

        as_json = json_translators.to_json(statement)
        decoded_statement = json_translators.from_json(as_json)

        self.assertEqual(statement, decoded_statement)

    def test_json_proof(self):
        conclusion = TruthStatement("A and B")
        hypotheses = {TruthStatement("A"), TruthStatement("B")}
        proof = Proof(conclusion=conclusion,
                      hypotheses=hypotheses,
                      justification="Obvious")

        as_json = json_translators.to_json(proof)
        decoded_proof = json_translators.from_json(as_json)

        self.assertEqual(proof, decoded_proof)

    def test_empty_truth_tracer(self):
        truth_tracer = TruthTracer()

        as_json = json_translators.to_json(truth_tracer)
        decoded_truth_tracer = json_translators.from_json(as_json)

        self.assertEqual(TruthTracer, type(decoded_truth_tracer))
        self.assertEqual(truth_tracer, decoded_truth_tracer)

    def test_truth_tracer_with_truths(self):
        truth_tracer = TruthTracer()
        truth_statement = TruthStatement("I am Batman")
        truth_tracer.add_truth_statement(truth_statement)

        statements = truth_tracer._truth_statements
        statements_as_json = json_translators.to_json(statements, indent=4)
        decoded_statements = json_translators.from_json(statements_as_json)
        self.assertEqual(statements, decoded_statements)

        truth_tracer_as_json = json_translators.to_json(truth_tracer)
        decoded_truth_tracer = json_translators.from_json(truth_tracer_as_json)
        self.assertEqual(decoded_truth_tracer, truth_tracer)

    def test_proof_map(self):

        A = TruthStatement("A")
        proof = Proof(A)

        map = [Proof(A), Proof(A)]

        set1 = {Proof(A), Proof(A)}
        set2 = {Proof(A), Proof(A)}
        print(set1.union(set2))
        self.assertEqual(set1, set2)
        print(json_translators.to_json(map, indent=4))

    def test_truth_tracer_with_proof(self):
        truth_statement = TruthStatement("A")
        truth_tracer = TruthTracer(truth_statements=truth_statement,
                                   proofs=Proof(truth_statement))

        as_json = json_translators.to_json(truth_tracer, indent=4)

        print("===========")
        print("as json:", as_json)
        print("before:", truth_tracer)
        print("proofs's:", truth_tracer._proofs)
        for k, proof_list in truth_tracer._proofs.items():
            print("proof key:", type(k), k)
            print("proof_list:", type(proof_list), proof_list)
            for proof in proof_list:
                print("\t", proof)
        print()
        decoded_truth_tracer = json_translators.from_json(as_json)
        print("after:", decoded_truth_tracer)
        print("proofs's:", decoded_truth_tracer._proofs)
        for k, proof_list in decoded_truth_tracer._proofs.items():
            print("key:", type(k), k)
            print("proof_list:", type(proof_list), proof_list)
            for proof in proof_list:
                print("\t", proof)

        self.assertEqual(truth_tracer._truth_statements, decoded_truth_tracer._truth_statements)
        self.assertEqual(truth_tracer._proofs, decoded_truth_tracer._proofs)
        self.assertEqual(TruthTracer, type(decoded_truth_tracer))
        self.assertEqual(truth_tracer, decoded_truth_tracer)


@unittest.skip
class StorageTest(unittest.TestCase):

    def test(self):
        storage = database.TruthStatementStorage()
        truth_statement = TruthStatement("This is my statement")
        storage.write_truth_statement(truth_statement)
        storage.save()

        second_storage = database.TruthStatementStorage()
        second_storage.load()
        self.assertEqual(second_storage.read(truth_statement.uuid),
                         truth_statement)

    def test_save(self):
        storage = database.TruthStatementStorage()
        storage.load()
        storage.write_truth_statement(TruthStatement("This is my statement"))
        storage.save()

        second_storage = database.TruthStatementStorage()
        second_storage.load()

        print(second_storage.read_all())


if __name__ == '__main__':
    unittest.main()
