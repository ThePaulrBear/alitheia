# -*- coding: utf-8 -*-
"""
Created on Mon Dec 8 2019

@author: Paul Wintz
"""
import unittest, sys
from uuid import uuid4

import sympy
from sympy import Symbol
from sympy.parsing.sympy_parser import parse_expr

from statement import TruthStatement, Proof, Definition
from truth_tracer import TruthTracer, UnknownTruthStatementError, ContradictionError

# Decrease the recursion limit so that tests fail quickly if stuck in infinite recursion.
sys.setrecursionlimit(100)


class TestSandbox(unittest.TestCase):

    def test(self):
        A = TruthStatement("A")
        B = TruthStatement("B")
        print(sympy.simplify_logic((A&B) >> B))

    def test_to_cnf(self):

        A = Symbol("A")
        B = Symbol("B")
        pathological = Symbol("A B")
        # parsed = parse_expr(str(pathological))
        # print(parsed)

        C = sympy.Symbol("C")
        ts = TruthStatement("Hello")
        s = "a" + str(uuid4().hex)
        uuidsym = sympy.Symbol(s)

        expression = A & A | (~B & C) & ~B
        print("expression:", expression)
        print("expression atoms:", expression.atoms())
        print("cnf:", sympy.to_cnf(expression))
        print("cnf simplified:", sympy.to_cnf(expression, simplify=True))
        print("dnf:", sympy.to_dnf(expression))
        print("dnf simplified:", sympy.to_dnf(expression, simplify=True))

        print(uuid4().hex)

        print("ts:", ts)
        expr = parse_expr(ts.uuid + "|" + s)
        print(expr)
        print(type(expr))
        print(sympy.Or.mro())


class TestInit(unittest.TestCase):

    def test_init_default(self):
        tt = TruthTracer()

        self.assertEqual(len(tt._proofs), 0)
        self.assertEqual(len(tt._truth_statements), 0)

    def test_init_with_truths_in_a_set(self):
        A = TruthStatement("A")
        tt = TruthTracer(truth_statements={A})

        self.assertIn(A.uuid, tt._truth_statements)
        self.assertEqual({A.uuid: A}, tt._truth_statements)

    def test_init_with_truths_in_a_list(self):
        A = TruthStatement("A")
        tt = TruthTracer(truth_statements=[A])

        self.assertIn(A.uuid, tt._truth_statements)
        self.assertEqual({A.uuid: A}, tt._truth_statements)


class TestAddTruth(unittest.TestCase):

    def test_add_truth_statement(self):
        A = TruthStatement("A")
        tt = TruthTracer()
        tt.add_truth_statement(A)

        self.assertEqual(tt._truth_statements, {A.uuid: A})


class TestCheckDependency(unittest.TestCase):

    def test_unknown_truth_statement(self):
        tt = TruthTracer()

        A = TruthStatement("A")
        with self.assertRaises(UnknownTruthStatementError):
            tt._check_dependency(A)

    def test_known_statement(self):
        A = TruthStatement("A")
        tt = TruthTracer(truth_statements=A)

        tt._check_dependency(A)

    def test_composite_statement_including_unknown_statement(self):
        A = TruthStatement("A")
        B = TruthStatement("B")

        tt = TruthTracer(truth_statements={A})

        with self.assertRaises(UnknownTruthStatementError):
            tt._check_dependency(A & B)

        with self.assertRaises(UnknownTruthStatementError):
            tt._check_dependency(A | B)

    def test_composite_statement_of_known_statements(self):
        A = TruthStatement("A")
        B = TruthStatement("B")

        tt = TruthTracer(truth_statements={A, B})

        tt._check_dependency(A & B)


class TestCheckProofDependencies(unittest.TestCase):

    def test_missing_conclusion(self):
        A = TruthStatement("A")
        proof = Proof(conclusion=A)

        tt = TruthTracer()

        with self.assertRaises(UnknownTruthStatementError):
            tt._check_proof_dependencies(proof)

    def test_missing_hypothesis(self):
        conclusion = TruthStatement("A")
        hypothesis = TruthStatement("B")
        proof = Proof(conclusion=conclusion, hypotheses=hypothesis)

        tt = TruthTracer(truth_statements=conclusion)

        with self.assertRaises(UnknownTruthStatementError):
            tt._check_proof_dependencies(proof)

    def test_missing_compound_conclusion(self):
        conclusion_1 = TruthStatement("A")
        conclusion_2 = TruthStatement("B")
        hypothesis = TruthStatement("C")
        proof = Proof(conclusion=conclusion_1 & conclusion_2, hypotheses=hypothesis)

        tt = TruthTracer(truth_statements={conclusion_1, hypothesis})

        with self.assertRaises(UnknownTruthStatementError):
            tt._check_proof_dependencies(proof)

    def test_missing_compound_hypothesis(self):
        conclusion = TruthStatement("A")
        hypothesis_1 = TruthStatement("B")
        hypothesis_2 = TruthStatement("C")
        proof = Proof(conclusion=conclusion, hypotheses=hypothesis_1 | hypothesis_2)

        tt = TruthTracer(truth_statements={conclusion, hypothesis_1})

        with self.assertRaises(UnknownTruthStatementError):
            tt._check_proof_dependencies(proof)

    def test_missing_multiple_hypothesis(self):
        conclusion = TruthStatement("A")
        hypothesis_1 = TruthStatement("B")
        hypothesis_2 = TruthStatement("C")
        proof = Proof(conclusion=conclusion, hypotheses=hypothesis_1 & hypothesis_2)

        tt = TruthTracer(truth_statements={conclusion, hypothesis_1})

        with self.assertRaises(UnknownTruthStatementError):
            tt._check_proof_dependencies(proof)

    def test_compound_conclusion_and_hypotheses(self):
        conclusion_1 = TruthStatement("A")
        conclusion_2 = TruthStatement("B")
        hypothesis_1 = TruthStatement("C")
        hypothesis_2 = TruthStatement("D")
        proof = Proof(conclusion=conclusion_1 & conclusion_2,
                      hypotheses=hypothesis_1 & (~hypothesis_1 | hypothesis_2))

        tt = TruthTracer(truth_statements={conclusion_1, conclusion_2, hypothesis_1, hypothesis_2})

        tt._check_proof_dependencies(proof)


class TestAddProof(unittest.TestCase):

    def test_cannot_add_proof_of_unknown_truth_statement(self):
        A = TruthStatement("A")
        proof = Proof(A)
        tt = TruthTracer()

        with self.assertRaises(UnknownTruthStatementError):
            tt.add_proof(proof)

    def test_add_proof_of_known_statement(self):
        A = TruthStatement("A")
        tt = TruthTracer()
        tt.add_truth_statement(A)

        proof = Proof(A)
        tt.add_proof(proof)

        self.assertEqual(tt._proofs[A], {proof})

    def test_add_multiple_proofs_of_known_statement(self):
        A = TruthStatement("A")
        proof_1 = Proof(A)
        proof_2 = Proof(A)
        tt = TruthTracer(truth_statements=A,
                         proofs={proof_1, proof_2})

        self.assertEqual(tt._proofs[A], {proof_1, proof_2})

    def test_add_proof_of_composite_statement(self):
        A = TruthStatement("A")
        B = TruthStatement("B")

        tt = TruthTracer(truth_statements={A, B})

        tt.add_proof(Proof(conclusion=A & B, hypotheses=A | B))


class TestSanitizeGivens(unittest.TestCase):

    def test_various(self):
        A = TruthStatement("A")
        B = TruthStatement("B")
        C = TruthStatement("C")
        self.assertEqual(TruthTracer._sanitize_givens(None), sympy.true)
        self.assertEqual(TruthTracer._sanitize_givens(A & B), A & B)
        self.assertEqual(TruthTracer._sanitize_givens(A & B & A & B), A & B)
        self.assertEqual(TruthTracer._sanitize_givens(A & B & ~C), A & B & ~C)


class TestSubstituteGivens(unittest.TestCase):

    def test_single_substitution(self):
        A = Symbol("A")
        B = Symbol("B")
        self.assertTrue(TruthTracer._substitute_givens(A | B, A))
        self.assertEqual(TruthTracer._substitute_givens(A & B, A), B)

    def test_substitute_conjunction(self):
        A = TruthStatement("A")
        B = TruthStatement("B")
        self.assertEqual(TruthTracer._substitute_givens(A, A & B), sympy.true)
        self.assertEqual(TruthTracer._substitute_givens(B, A & B), sympy.true)
        self.assertEqual(TruthTracer._substitute_givens(A & B, A & B), sympy.true)

        print(TruthTracer._substitute_givens(B, A & B))

    def test_substitute_disjunction(self):
        A = Symbol("A")
        B = Symbol("B")
        self.assertEqual(TruthTracer._substitute_givens(A, A | B), A)
        self.assertEqual(TruthTracer._substitute_givens(A & B, A | B), A & B)
        self.assertEqual(TruthTracer._substitute_givens(A | B, A | B), sympy.true)
        self.assertEqual(TruthTracer._substitute_givens((A & A) | B, A | B), sympy.true)


# noinspection PyPep8Naming
class TestIsJustifiedBasics(unittest.TestCase):

    def test_given_statement_is_justified(self):
        A = TruthStatement("A")
        truth_tracer = TruthTracer(truth_statements=A)

        self.assertTrue(truth_tracer.is_justified(conclusion=A, givens=A))

    def test_unsupported_statement(self):
        unsupported = TruthStatement("Epstein faked the Moon landing.")
        truth_tracer = TruthTracer(truth_statements=unsupported)

        self.assertFalse(truth_tracer.is_justified(conclusion=unsupported, givens=None))

    def test_contradiction(self):
        A = TruthStatement("A")
        truth_tracer = TruthTracer(truth_statements=A)

        with self.assertRaises(ContradictionError):
            truth_tracer.is_justified(conclusion=~A, givens=A)

        with self.assertRaises(ContradictionError):
            truth_tracer.is_justified(conclusion=A, givens=~A)

    def test_recursion(self):
        A = TruthStatement("A")
        B = TruthStatement("B")
        C = TruthStatement("C")

        truth_tracer = TruthTracer(truth_statements={A, B, C})
        truth_tracer.add_proof(Proof(conclusion=B, hypotheses=C))
        truth_tracer.add_proof(Proof(conclusion=A, hypotheses=B))

        self.assertFalse(truth_tracer.is_justified(A, givens=None))
        self.assertFalse(truth_tracer.is_justified(B, givens=A))
        self.assertTrue(truth_tracer.is_justified(A, givens=B))
        self.assertTrue(truth_tracer.is_justified(B, givens=C))
        self.assertTrue(truth_tracer.is_justified(A, givens=C))

    def test_proof_loop(self):
        A = TruthStatement("A")
        B = TruthStatement("B")

        truth_tracer = TruthTracer(truth_statements={A, B})

        truth_tracer.add_proof(Proof(conclusion=A, hypotheses=B))
        truth_tracer.add_proof(Proof(conclusion=B, hypotheses=A))

        self.assertTrue(truth_tracer.is_justified(A, givens=None))


class TestIsJustifiedContradictions(unittest.TestCase):
    pass


class TestIsJustifiedCompoundConclusion(unittest.TestCase):

    def test_conjunction(self):
        A = TruthStatement("A")
        B = TruthStatement("B")
        C = TruthStatement("C")

        proof_of_A = Proof(conclusion=A, hypotheses=C)
        proof_of_B = Proof(conclusion=B, hypotheses=C)
        truth_tracer = TruthTracer(truth_statements={A, B, C},
                                   proofs={proof_of_A, proof_of_B})

        self.assertTrue(truth_tracer.is_justified(A & B, givens=C))

    def test_disjunction(self):
        A = TruthStatement("A")
        B = TruthStatement("B")

        truth_tracer = TruthTracer()

        # self.assertFalse(truth_tracer.is_justified(A | B, givens=None))
        self.assertTrue(truth_tracer.is_justified(A | B, givens=A))
        # self.assertTrue(truth_tracer.is_justified(A | B, givens=B))

    def test_negation(self):
        A = TruthStatement("A")
        B = TruthStatement("B")
        truth_tracer = TruthTracer(truth_statements={A, B})
        truth_tracer.add_proof(Proof(conclusion=~A, hypotheses=B))

        with self.assertRaises(ContradictionError):
            truth_tracer.is_justified(conclusion=A, givens=B)

        self.assertTrue(truth_tracer.is_justified(conclusion=~A, givens=B))


class TestIsJustifiedCompoundHypotheses(unittest.TestCase):

    def test_conjunction(self):
        A = TruthStatement("A")
        B = TruthStatement("B")
        C = TruthStatement("C")

        proof = Proof(conclusion=C, hypotheses=A & B)
        truth_tracer = TruthTracer(truth_statements={A, B, C}, proofs=proof)

        self.assertFalse(truth_tracer.is_justified(C, givens=None))
        self.assertFalse(truth_tracer.is_justified(C, givens=A))
        self.assertTrue(truth_tracer.is_justified(C, givens=A & B))

    def test_disjunction(self):
        A = TruthStatement("A")
        B = TruthStatement("B")
        C = TruthStatement("C")

        truth_tracer = TruthTracer(truth_statements={A, B, C})
        truth_tracer.add_proof(Proof(conclusion=A, hypotheses=B | C))

        self.assertFalse(truth_tracer.is_justified(A, givens=None))

        # Only one of the hypotheses need to be given.
        self.assertTrue(truth_tracer.is_justified(A, givens=B))
        self.assertTrue(truth_tracer.is_justified(A, givens=C))
        self.assertTrue(truth_tracer.is_justified(A, givens=B & C))

        # If one hypothesis is true, then the other can be false.
        self.assertTrue(truth_tracer.is_justified(A, givens=B & ~C))


class TestIsJustifiedCompoundProofConclusion(unittest.TestCase):

    def test_disjunction(self):
        A = TruthStatement("A")
        B = TruthStatement("B")
        C = TruthStatement("C")

        proof = Proof(A | B, hypotheses=C)
        truth_tracer = TruthTracer(truth_statements={A, B, C}, proofs=proof)

        print(truth_tracer._proofs)

        # self.assertFalse(truth_tracer.is_justified(A, givens=C))
        # self.assertFalse(truth_tracer.is_justified(B, givens=C))
        self.assertTrue(truth_tracer.is_justified(A | B, givens=C))

    def test_conjunction(self):
        A = TruthStatement("A")
        B = TruthStatement("B")
        C = TruthStatement("C")

        proof = Proof(conclusion=A & B, hypotheses=C)
        truth_tracer = TruthTracer(truth_statements={A, B, C}, proofs=proof)

        self.assertTrue(truth_tracer.is_justified(A & B, givens=C))


class TestIsJustifiedCompoundGiven(unittest.TestCase):

    def test_conjunction(self):
        A = TruthStatement("A")
        B = TruthStatement("B")
        C = TruthStatement("C")

        truth_tracer = TruthTracer(truth_statements={A, B, C})

        self.assertTrue(truth_tracer.is_justified(A, givens=A & B & C))
        self.assertTrue(truth_tracer.is_justified(B, givens=A & B & C))
        self.assertTrue(truth_tracer.is_justified(C, givens=A & B & C))

    def test_disjunction(self):
        A = TruthStatement("A")
        B = TruthStatement("B")
        C = TruthStatement("C")

        A_implies_C = Proof(C, hypotheses=A)
        B_implies_C = Proof(C, hypotheses=B)
        truth_tracer = TruthTracer(truth_statements={A, B, C},
                                   proofs={A_implies_C, B_implies_C})

        self.assertFalse(truth_tracer.is_justified(A, givens=A | B))
        self.assertFalse(truth_tracer.is_justified(B, givens=A | B))
        self.assertTrue(truth_tracer.is_justified(C, givens=A | B))

        with self.assertRaises(ContradictionError):
            truth_tracer.is_justified(C, givens=(A | B) & ~C)

    def test_negation(self):
        A = TruthStatement("A")
        B = TruthStatement("B")
        C = TruthStatement("C")

        truth_tracer = TruthTracer(truth_statements={A, B, C}, proofs={})

        # Negation implies negation
        self.assertTrue(truth_tracer.is_justified(~A, givens=~A))

        with self.assertRaises(ContradictionError):
            truth_tracer.is_justified(A, givens=~A)


class TestIsJustifiedOther(unittest.TestCase):

    def test_negation(self):

        A = TruthStatement("A")
        B = TruthStatement("B")
        truth_tracer = TruthTracer(truth_statements={A, B})

        truth_tracer.add_proof(Proof(conclusion=~A, hypotheses=B))
        truth_tracer.add_proof(Proof(conclusion=A, hypotheses=~B))

        with self.assertRaises(ContradictionError):
            truth_tracer.is_justified(conclusion=A, givens=B)

        with self.assertRaises(ContradictionError):
            self.assertFalse(truth_tracer.is_justified(conclusion=~A, givens=~B))

        self.assertTrue(truth_tracer.is_justified(conclusion=~A, givens=B))
        # self.assertTrue(truth_tracer.is_justified(conclusion=A, givens=~B))
        #
        # # The is_justified method does not care that the givens give rise to a contradiction.
        # self.assertTrue(truth_tracer.is_justified(B, givens={A, B}))

    def test_multiple_proofs(self):
        A = TruthStatement("A")
        B = TruthStatement("B")
        C = TruthStatement("C")

        truth_tracer = TruthTracer(truth_statements={A, B, C})
        truth_tracer.add_proof(Proof(conclusion=A, hypotheses=B))
        truth_tracer.add_proof(Proof(conclusion=A, hypotheses=C))

        self.assertFalse(truth_tracer.is_justified(A, givens=None))
        self.assertTrue(truth_tracer.is_justified(A, givens=B))
        self.assertTrue(truth_tracer.is_justified(A, givens=C))
        self.assertTrue(truth_tracer.is_justified(A, givens=B & C))

    def test_trump_inauguration_proof(self):
        truth_tracer = TruthTracer()

        inauguration_image = Definition("The Image", "An photograph the crowds at ###Trump's Inauguration### seen <here>")

        image_of_inauguration_crowd = TruthStatement("The Trump White House released ###The Image###")
        camera_location_crowd = TruthStatement("###The Image### was taken at the capitol steps from an altitude of H")
        google_maps = TruthStatement("Google Maps shows the distance from the Capitol is D feet")
        optical_path = TruthStatement("The path of light rays are well approximated by straight lines")
        average_man_height = TruthStatement("The average height of an American male is H")
        law_of_similar_triangles = TruthStatement("If two triangles are equal, then the ratios "
                                                  "between corresponding sides are equal.")

        crowd_size = TruthStatement("The crowd at Trump's Inauguration extended to point X.")
        crowd_size_proof = Proof(crowd_size, hypotheses=image_of_inauguration_crowd & camera_location_crowd
                                                        & google_maps & optical_path & average_man_height
                                                        & law_of_similar_triangles)


# @unittest.skip
class TestLawsOfLogic(unittest.TestCase):

    def test_deMorgans_law(self):
        A = TruthStatement("A")
        B = TruthStatement("B")

        truth_tracer = TruthTracer(truth_statements={A, B})

        self.assertTrue(truth_tracer.is_justified(~A & ~B, givens=~(A | B)))
        self.assertTrue(truth_tracer.is_justified(~(A & B), givens=~A | ~B))
        self.assertTrue(truth_tracer.is_justified(~A | ~B, givens=~(A & B)))
        self.assertTrue(truth_tracer.is_justified(~(A | B), givens=~A & ~B))

    def test_contrapositive(self):
        A = TruthStatement("A")
        B = TruthStatement("B")

        truth_tracer = TruthTracer(truth_statements={A, B})
        truth_tracer.add_proof(Proof(conclusion=A, hypotheses=B))

        self.assertTrue(truth_tracer.is_justified(~B, givens=~A))

        with self.assertRaises(ContradictionError):
            self.assertTrue(truth_tracer.is_justified(B, givens=~A))


class TestEquals(unittest.TestCase):

    def test_wrong_type(self):
        tt = TruthTracer()
        self.assertNotEqual(tt, object())
        self.assertFalse(tt == 3)
        self.assertFalse(3 == tt)

    def test_default(self):
        tt1 = TruthTracer()
        tt2 = TruthTracer()

        self.assertEqual(tt1, tt2)

    def test_with_added_truth_statement(self):
        tt1 = TruthTracer()
        tt2 = TruthTracer()

        statement = TruthStatement("Truth is truth.")

        tt1.add_truth_statement(statement)

        self.assertNotEqual(tt1, tt2)

        tt2.add_truth_statement(statement)

        self.assertEqual(tt1, tt2)

    def test_with_added_proof(self):

        statement = TruthStatement("Truth is truth.")
        tt1 = TruthTracer(truth_statements=statement)
        tt2 = TruthTracer(truth_statements=statement)

        proof = Proof(statement)
        tt1.add_proof(proof)

        self.assertNotEqual(tt1, tt2)

        tt2.add_proof(proof)

        self.assertEqual(tt1, tt2)


if __name__ == '__main__':
    unittest.main()
