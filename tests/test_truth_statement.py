import unittest
from uuid import uuid4

import sympy

from statement import TruthStatement


class TestTruthStatement(unittest.TestCase):

    def test_class(self):
        class BaseClass:
            def __new__(cls, *args, **kwargs):
                print("This is in BaseClass.__new__")
                return super(BaseClass, cls).__new__(cls)

            def __init__(self):
                print("this is in BaseClass.__init__")

        class Subclass(BaseClass):
            def __new__(cls, *args, **kwargs):
                print("This is in Subclass.__new__")
                return super(Subclass, cls).__new__(cls)

            def __init__(self):
                print("this is in Subclass.__init__")
                BaseClass.__init__(self)

        Subclass()

    def test_boolean(self):
        atom = sympy.AtomicExpr()
        # print(atom.subs({atom: 1}))
        # print(type(atom))
        # a = sympy.symbol.Boolean("a")
        # b = sympy.symbol.Boolean("b")
        # print(a & b)

        A = TruthStatement("A")
        B = TruthStatement("B")
        C = TruthStatement("C")

        print(~A & ~B)
        # exp = sympy.parsing.sympy_parser.parse_expr("A | B", {"A": A, "B":B})
        # print(exp.subs(A, C))

    def test_uuid(self):
        class UniqueObject:
            def __init__(self, a, b=None):
                if b is not None:
                    self.uuid = uuid4()

        obj_1 = UniqueObject("A", "B")
        obj_2 = UniqueObject("A", "B")

        print("obj_1:", id(obj_1), obj_1.uuid)
        print("obj_2:", id(obj_2), obj_2.uuid)

        assert obj_1 is not obj_2
        assert not obj_1 == obj_2

    def test_init(self):
        statement = TruthStatement("Blah blah blah")
        self.assertEqual(statement.text, "Blah blah blah")
        self.assertIsInstance(statement.uuid, str)

    def test_unique_uuids(self):
        ts_1 = TruthStatement("A", uuid=uuid4())
        ts_2 = TruthStatement("B", uuid=uuid4())
        print(id(ts_1))
        print(id(ts_2))

        print(ts_1.__dict__)

        self.assertNotEqual(ts_1.uuid, ts_2.uuid)

    def test_same_uuids_specified(self):
        uuid = uuid4()
        ts_1 = TruthStatement("", uuid=uuid)
        ts_2 = TruthStatement("", uuid=uuid)
        self.assertEqual(ts_1.uuid, ts_2.uuid)

    def test_unique_uuids_specified(self):
        uuid_1 = uuid4()
        uuid_2 = uuid4()
        ts_1 = TruthStatement("", uuid=uuid_1)
        ts_2 = TruthStatement("", uuid=uuid_2)
        self.assertNotEqual(ts_1.uuid, ts_2.uuid)
        self.assertNotEqual(ts_1, ts_2)

    def test_eq_self(self):
        statement = TruthStatement("We hold these truths")
        self.assertEqual(statement, statement)

    def test_eq_same_text_diff_uuid_1(self):
        same_text_diff_uuid_1 = TruthStatement("Same text, different UUID", uuid4())
        same_text_diff_uuid_2 = TruthStatement("Same text, different UUID", uuid4())

        self.assertNotEqual(same_text_diff_uuid_1, same_text_diff_uuid_2)
        self.assertNotEqual(same_text_diff_uuid_2, same_text_diff_uuid_1)

    def test_eq_same_text_and_uuid(self):
        uuid = uuid4()
        same_text_and_uuid_1 = TruthStatement("hello", uuid=uuid)
        same_text_and_uuid_2 = TruthStatement("hello", uuid=uuid)

        self.assertEqual(same_text_and_uuid_1, same_text_and_uuid_2)

    # This test is disabled because there should NOT be two distinct instances
    # with the same uuid.
    # def test_eq_diff_text_same_uuid(self):
    #     uuid = uuid4()
    #     diff_text_same_uuid_1 = TruthStatement("What's", uuid=uuid)
    #     diff_text_same_uuid_2 = TruthStatement("Up?", uuid=uuid)
    #
    #     self.assertNotEqual(diff_text_same_uuid_1, diff_text_same_uuid_2)