from typing import Union, Set, Dict, Optional

import sympy
from sympy.logic.boolalg import Boolean, BooleanTrue

from statement import TruthStatement, Proof


class TruthTracer:
    TruthSet = Union[TruthStatement, Set[TruthStatement]]

    # A dictionary maps from a uuid string to a TruthStatement.
    # We need this in order to map from UUID to TruthStatement when
    # reconstructing from JSON.
    _truth_statements: Dict[str, TruthStatement]
    _proofs: Dict[TruthStatement, Set[Proof]]
    # _definitions: Dict[UUID, Definition]

    def __init__(self, truth_statements=None, proofs=None, definitions=None):
        if truth_statements is None:
            self._truth_statements = {}
        elif isinstance(truth_statements, dict):
            self._truth_statements = truth_statements
        elif isinstance(truth_statements, list) or isinstance(truth_statements, set):
            self._truth_statements = {}
            for ts in truth_statements:
                self.add_truth_statement(ts)
        elif isinstance(truth_statements, TruthStatement):
            self._truth_statements = {truth_statements.uuid: truth_statements}
        else:
            raise ValueError("truth_statements must be a set, list, dictionary, or TruthStatement, was a %s."
                             % type(truth_statements))

        if proofs is None:
            self._proofs = {}
        elif isinstance(proofs, dict):
            for key, value in proofs.items():
                # if isinstance(key, str):
                #     ts = self._truth_statements[key]
                assert isinstance(key, TruthStatement)
                assert isinstance(value, set), "value was not a set: %s" % type(value)
            self._proofs = proofs
        elif isinstance(proofs, set) or isinstance(proofs, list):
            self._proofs = {}
            for value in proofs:
                if isinstance(value, list):
                    for proof in value:
                        self.add_proof(proof)
                elif isinstance(value, Proof):
                    assert isinstance(value.conclusion, TruthStatement)
                    self.add_proof(value)
        elif isinstance(proofs, Proof):
            self._proofs = {}
            self.add_proof(proofs)
        else:
            raise ValueError("proofs must be a dictionary or a set. Was a %s"
                             % type(proofs))

        if definitions is None:
            self._definitions = {}
        else:
            self._definitions = definitions

    def get_truth_statements(self) -> Set[TruthStatement]:
        return set(self._truth_statements.values())

    def get_all_proofs(self) -> Set[Proof]:
        all_proofs = set()
        for proof_list in self._proofs.values():
            all_proofs |= set(proof_list)
        return all_proofs

    def get_proofs(self, truth_statement: TruthStatement) -> Set[Proof]:
        # Get the proofs, if available, otherwise return empty set.
        return self._proofs.get(truth_statement, set())

    def add_proof(self, proof: Proof):
        assert isinstance(proof, Proof), "Not a proof: %s" % type(proof)
        # assert isinstance(proof.conclusion, TruthStatement), \
        #     "Conclusion not a TruthStatement: %s" % type(proof.conclusion)
        self._check_proof_dependencies(proof)

        conclusion = sympy.to_cnf(proof.conclusion)

        if isinstance(conclusion, sympy.And):
            for c in conclusion.args:
                self._proofs.setdefault(c, set())
                self._proofs[c].add(proof)

        self._proofs.setdefault(proof.conclusion, set())
        self._proofs[proof.conclusion].add(proof)

    def add_truth_statement(self, truth_statement: TruthStatement):
        self._truth_statements[truth_statement.uuid] = truth_statement
        # self._truth_statements.add(truth_statement)

    @staticmethod
    def _sanitize_givens(givens: Union[TruthStatement, Set[TruthStatement]] = None) -> TruthStatement:
        """
        Convert a set of TruthStatements into a standard form.
        :param givens: a set of TruthStatements
        :return:
        """
        if givens is None:
            return sympy.true

        assert not isinstance(givens, set), "givens must be a symbol"

        return sympy.simplify_logic(givens)
        # try:
        #     # Test if it is iterable
        #     iter(givens)
        # except TypeError:
        #     # If its not, then try to split up the conjunction.
        #     if isinstance(givens, sympy.And):
        #         return set(givens.args)
        #     # If it's not a conjunction, merely return the given as a set.
        #     return {givens}
        #
        # # If it is iterable, then try to split up each of the elements.
        # as_a_set = set()
        # for item in givens:
        #     if isinstance(item, sympy.And):
        #         as_a_set = as_a_set.union(item.args)
        #     else:
        #         as_a_set.add(item)
        # return as_a_set

    def is_justified(self,
                     conclusion: TruthStatement,
                     givens: Optional[TruthStatement],
                     test_negation=True):
        print("=== Starting is_justified ===\n\t"
              "conclusion: %s, \n\t"
              "    givens: %s " % (conclusion, givens))

        conclusion = sympy.simplify_logic(conclusion)
        givens = TruthTracer._sanitize_givens(givens)

        if conclusion == givens:
            print("Conclusion and givens are equal.")
            return True

        # Handle the case where several statements are AND'ed or OR'ed together
        if isinstance(conclusion, sympy.Or):
            disjunction_terms = conclusion.args
            for term in disjunction_terms:
                print("Analyzing the '%s' branch of the OR statements '%s'" % (term, disjunction_terms))
                # If any term in the conclusion is True, then the entire conclusion is true.
                if self.is_justified(term, givens, test_negation=False):
                    # test_negation=false is used here to prevent testing the contrapositive of each
                    return sympy.true
            return sympy.false
        elif isinstance(conclusion, sympy.And):
            conjunction_terms = conclusion.args
            for term in conjunction_terms:
                print("Analyzing the '%s' branch of the AND statements '%s'" % (term, conjunction_terms))
                # If any term in the conclusion is False, then the entire conclusion is false.
                if not self.is_justified(term, givens):
                    return sympy.false
            return sympy.true

        # We check the negation of the conclusion to verify that it is not justified. If it is,
        # then the conclusion would be a contradiction.
        if test_negation:
            is_negation_justified = False
            try:
                print("Testing negation --> ")
                is_negation_justified = self.is_justified(~conclusion, givens, test_negation=False)
            except ContradictionError:
                pass
            if is_negation_justified:
                raise ContradictionError("Negation is justified")

            # Test Contrapositive
            print("Testing contrapositive --> ")
            is_contrapositive_justified = self.is_justified(~givens, ~conclusion, test_negation=False)
            if is_contrapositive_justified:
                print("--> Contrapositive of %s justified" % conclusion)
                return True

        # if isinstance(givens, sympy.Or):
        #     for disjunct in givens.args:
        #         if not self.is_justified(conclusion, disjunct):
        #             print("--> One of the dijuncts in given disjunction does not justify conclusion")
        #             return False
        #     return True

        conclusion = TruthTracer._substitute_givens(conclusion, givens)
        if conclusion is sympy.true:
            print("--> Conclusion simplifies to true")
            return True

        if not self.has_proof(conclusion):
            print("--> No proofs. Conclusion %s not justified given %s" % (conclusion, givens))
            return False

        required_base_truths = sympy.false
        for proof in self.get_proofs(conclusion):
            print("proof:", proof)

            try:
                if self.is_justified(proof.hypotheses, givens):
                    if sympy.simplify_logic(proof.conclusion >> conclusion):
                        print("--> Conclusion justified by proof.")
                        return True
                    elif self.is_justified(conclusion, givens & proof.conclusion):
                        return True
                        # print("proof.conclusion:", proof.conclusion)
                        # print("conclusion:", conclusion)
                        # print("sympy.simplify_logic(proof.conclusion >> conclusion)", sympy.simplify_logic(proof.conclusion >> conclusion))
                else:
                    required_base_truths |= proof.hypotheses
                    pass
            except ContradictionError:
                print("Contradiction for proof: %s" % proof)
                pass

        # if givens :
        #     print("--> No givens. Conclusion not justified: %s" % conclusion)
        #     return False

        required_base_truths = self._substitute_givens(required_base_truths, givens)

        if required_base_truths is sympy.true:
            return True

        if required_base_truths is sympy.false:
            raise ContradictionError("Conclusion requires a contradiction.")

        return self.is_justified(required_base_truths, givens=givens)

    def has_proof(self, conclusion):
        return len(self.get_proofs(conclusion)) > 0

    @staticmethod
    def _substitute_givens(statement: Boolean, givens: Boolean):
        if isinstance(givens, sympy.And):
            for given in givens.args:
                statement = statement.subs(given, sympy.true)
                if statement is sympy.true:
                    return statement

        statement = statement.subs({givens: sympy.true})

        return statement

    def _check_proof_dependencies(self, proof: Proof):
        """ For the given proof, verify that the conclusion and hypotheses are
        composed of recognized TruthStatements. """

        self._check_dependency(proof.conclusion)
        self._check_dependency(proof.hypotheses)

    def _check_dependency(self, statement: Union[TruthStatement, Boolean]):
        if isinstance(statement, TruthStatement):
            # Check to make sure that the TruthStatement has previously been added to this TruthTracer.
            if statement.uuid not in self._truth_statements:
                raise UnknownTruthStatementError("Truth statement unknown: %s" % statement)
        elif isinstance(statement, sympy.Basic):
            # Split up composite statements, such as AND, OR, and NOT to make sure the individual statements,
            # (AKA "atoms") are known.
            for atom in statement.atoms():
                self._check_dependency(atom)
        else:
            raise ValueError("Not a statement. Type:%s" % type(statement))

    def __eq__(self, other):
        if not isinstance(other, TruthTracer):
            return False

        if not self._truth_statements == other._truth_statements:
            # print("ts differ")
            # print("this.ts", self._truth_statements)
            # print("other.ts", self._truth_statements)
            return False

        if not self._proofs == other._proofs:
            # print("proofs differ")
            return False

        return True

    def __str__(self):

        return "TruthTracer{TS's: %s, proofs: %s}" \
               % (len(self._truth_statements), len(self._proofs))


class UnknownTruthStatementError(ValueError):
    pass


class ContradictionError(ValueError):
    pass
